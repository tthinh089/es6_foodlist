export let renderFoodList = (foodArr) => {
  let contentHTML = "";
  foodArr.forEach((item) => {
    let { maMon, tenMon, loai, giaMon, khuyenMai, tinhTrang } = item;
    let contentTr = `<tr>
        <td>${maMon}</td>
        <td>${tenMon}</td>
        <td>${loai ? "Chay" : "Mặn"}</td>
        <td>${giaMon}</td>
        <td>${khuyenMai}</td>
        <td>${item.tinhGiaKM()}</td>
        <td>${tinhTrang ? "Còn" : "Hết"}</td>
        <td>
          <button class="btn btn-primary" onclick="handleEdit(${maMon})">Sửa</button>
          <button class="btn btn-danger" onclick="handleDelete(${maMon})">Xóa</button>
        </td>
    </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};
