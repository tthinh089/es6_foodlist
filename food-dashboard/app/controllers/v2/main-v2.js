import { renderFoodList } from "./controller-v2.js";
import { Food } from "../../models/v1/model.js";
import { layThongTinTuForm } from "../v1/controller-v1.js";

const BASE_URL = "https://64561d9a5f9a4f23613b1211.mockapi.io/food";

let fetchFoodList = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      let foodArr = res.data.map((item) => {
        let { id, name, type, discount, img, desc, price, status } = item;
        let food = new Food(id, name, type, price, discount, status, img, desc);
        return food;
      });
      renderFoodList(foodArr);
    })
    .catch((err) => {
      console.log(err);
    });
};

let handleDelete = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      fetchFoodList();
      console.log("res: ", res);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
window.handleDelete = handleDelete;

fetchFoodList();

window.handleAdd = () => {
  let data = layThongTinTuForm();
  let newFood = {
    name: data.tenMon,
    type: data.loai,
    discount: data.khuyenMai,
    img: data.hinhMon,
    desc: data.moTa,
    price: data.giaMon,
    status: data.tinhTrang,
  };
  axios({
    url: BASE_URL,
    method: "POST",
    data: newFood,
  })
    .then((res) => {
      console.log(res);
      $("#exampleModal").modal("hide");
    })
    .catch((err) => {
      console.log(err);
    });
};

window.handleEdit = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      $("#exampleModal").modal("show");
      let { id, type, price, img, status, desc, discount, name } = res.data;
      console.log(res.data);
      document.getElementById("foodID").value = id;
      document.getElementById("tenMon").value = name;
      document.getElementById("loai").value = type ? "loai1" : "loai2";
      document.getElementById("giaMon").value = price;
      document.getElementById("khuyenMai").value = discount;
      document.getElementById("tinhTrang").value = status ? "1" : "0";
      document.getElementById("hinhMon").value = img;
      document.getElementById("moTa").value = desc;
      return { id, name, type, price, discount, status, img, desc };
    })
    .catch((err) => {
      console.log(err);
    });
};
